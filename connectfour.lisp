(defconstant empty 0 "An empty square")
(defconstant red 1 "A red piece")
(defconstant yellow 2 "A yellow piece")

(defconstant height 6 "height of the board")
(defconstant width 7 "width of the board")


;; aux fns
(defun board-array ()
  (make-array (list width height) :initial-element empty))

(defun bref (board row col) (aref board row col))

(defun copy-board (board)
 (copy-seq board))

(defun print-board (board)
  (loop for row from 1 to height
       (format t "~& ~d " row)
       (loop for col from 1 to width

            )
       ))

(defun opponent (player) (if (eql player red)  yellow red))

;; game
(defun start (&optional comcolor )
  (let ((board (board-array)))
    ;;switched order to have the computer play first
    (if (eql comcolor red)
	(play board yellow)
	(play board red))))

(defun play (board player )
  ;;To-Do Check Function
  ;;Print function
(if (eql player yellow)
      (play (ai-move board) (opponent player))
      (play (player-move board) (opponent player))))

(defun legal-p (board row col)
  (cond ((> row maxheight) nil)
	((not (eql (bref board row col) 0) nil)
	 (1))))

(defun ai-move (board)
  (board))

(defun player-move (board)
  )
